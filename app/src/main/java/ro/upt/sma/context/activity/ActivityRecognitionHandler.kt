package ro.upt.sma.context.activity

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.content.Intent
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityRecognitionClient

class ActivityRecognitionHandler(context: Context) {

    private val client: ActivityRecognitionClient = ActivityRecognition.getClient(context)
    private val myIntent = Intent(context, ActivityRecognitionService::class.java)
    private val currentContext = context

    fun registerPendingIntent(): PendingIntent {
        val pendingIntent = PendingIntent.getBroadcast(currentContext, 111, myIntent, FLAG_UPDATE_CURRENT)
        // TODO 5: Create a pending intent for ActivityRecognitionService and register for updates to activity recognition client.
        client.requestActivityUpdates(10000, pendingIntent)

        return pendingIntent
    }

    fun unregisterPendingIntent(pendingIntent: PendingIntent) {
        client.removeActivityUpdates(pendingIntent)
    }

}
